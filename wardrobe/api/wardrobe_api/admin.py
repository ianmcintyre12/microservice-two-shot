from django.contrib import admin
from wardrobe_api.models import Location, Bin

# Register your models here.
@admin.register(Location)
class LocationAdmin(admin.ModelAdmin):
    list_display = (
        'closet_name',
        'section_number',
        'shelf_number',
    )

@admin.register(Bin)
class Bin(admin.ModelAdmin):
    list_display = (
        'closet_name',
        'bin_number',
        'bin_size',
    )
