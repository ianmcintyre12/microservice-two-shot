from django.db import models

class BinVO(models.Model):
    import_href = models.CharField(max_length=200, null=True, unique=True)
    closet_name = models.CharField(max_length=20)
    bin_number = models.PositiveSmallIntegerField(null=True)
    bin_size = models.PositiveSmallIntegerField(null=True)

class Shoe(models.Model):
    manufacturer = models.CharField(max_length=200)
    name = models.CharField(max_length=200)
    color = models.CharField(max_length=20)
    photo = models.URLField(null=True)
    bin = models.ForeignKey(
        BinVO,
        related_name = "shoe",
        null=True,
        on_delete=models.CASCADE,
    )

# Create your models here.
