# Wardrobify

Team:

* Ian - Hats microservice
* Kevin- Shoes Service

## Design

## Shoes microservice
The Shoe and BinVO model was created and there can be multiple shoes for one bin.
Since the bin data is stored in the wardrobe micro-service, the shoes microservice should not be able to make changes to the bin data. Therefore, a polling microservice was made to create instances of the BinVO for the shoe microservice.
The models and wardrobe microservice was integrated together to create an application using React by implementing RESTful apis for getting, creating, and deleting shoes.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

Planning to make a hat model using these properties:

Fabric : CharField, fabric name
Style name: CharField, name of style
Color : CharField, name of color
Picture URL : URLField , link to picture of hat most likely auto-generated by API
location : Foreignkey to LocationVO object that will be linked to Location objects from wardrobe API by ID
