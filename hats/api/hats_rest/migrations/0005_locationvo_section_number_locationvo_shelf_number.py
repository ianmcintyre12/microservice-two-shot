# Generated by Django 4.0.3 on 2023-06-02 17:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0004_hat_location'),
    ]

    operations = [
        migrations.AddField(
            model_name='locationvo',
            name='section_number',
            field=models.IntegerField(null=True),
        ),
        migrations.AddField(
            model_name='locationvo',
            name='shelf_number',
            field=models.IntegerField(null=True),
        ),
    ]
