# Generated by Django 4.0.3 on 2023-06-01 22:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hat',
            name='picture_url',
            field=models.URLField(blank=True),
        ),
    ]
